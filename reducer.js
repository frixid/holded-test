"use strict";

import ActionTypes from "./constants/actionTypes";

const initialState = {
  isStart: false,
  allCrypto:[]
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SHOW_BC_LIST:
      return {
        ...state,
        isStart: true,
        allCrypto: action.payload
      };

    default:
      return state;
  }
}
