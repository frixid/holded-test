'use strict'

import ActionTypes from './constants/actionTypes'
import API from "./lib/api";

export const showList = () => {
  return dispatch => {
    // dispatch({
    //   type:ActionTypes.SHOW_BC_LIST
    // })

    API.getCripto()
    .then(jsonResponse =>
    dispatch({
      type:ActionTypes.SHOW_BC_LIST,
//       payload:[
//   {name: 'Dssevin'},
//   {name: 'Dan'},
// ]
  payload:jsonResponse
    })
    // console.log(jsonResponse)
  )
    .catch(
      error => {console.log("hola");}
    )
  }
}
export const showFav = (value) => {
  return dispatch => {
    dispatch({
      type:ActionTypes.SHOW_BC_FAV,
    })
  }
}

export const showTop = (value) => {
  return dispatch => {
    dispatch({
      type:ActionTypes.SHOW_BC_TOP,
    })
  }
}

export const addFav = (value) => {
  return dispatch => {
    dispatch({
      type:ActionTypes.ADD_BC_FAV,
    })
  }
}

export const delFav = (value) => {
  return dispatch => {
    dispatch({
      type:ActionTypes.DEL_BC_FAV,
    })
  }
}
