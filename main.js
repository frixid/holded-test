import {registerRootComponent} from 'expo';
import React from 'react';
import {Provider} from 'react-redux'
import {createStore,applyMiddleware,combineReducers} from 'redux'
// import {persistStore,persistReducer} from 'redux-persist'
// import storage from 'redux-persist/lib/storage'
import ListScreen from './components/bc_list'
import FavouritesScreen from './components/bc_fav'
import TopScreen from './components/bc_top'
import Header from './components/Header'

import OpenDrawer from './components/openDrawer'

import reducers from './constants/reducers'
import thunkMiddleware from 'redux-thunk';
import {composeWithDevTools} from 'remote-redux-devtools';

import {SafeAreaView, Button, View, Text, Dimensions, ScrollView} from 'react-native'
import {createAppContainer, withNavigation } from 'react-navigation'
import {DrawerItems,NavigationDrawerProp,createDrawerNavigator} from 'react-navigation-drawer' //align
import { Icon } from 'react-native-elements'

const store = createStore(
  combineReducers(
    {...reducers},
  ),
// applyMiddleware(thunkMiddleware)
   composeWithDevTools(applyMiddleware(thunkMiddleware))
 )


 // const Stack = {
 //     FirstView: {
 //         screen: Login,
 //         navigationOptions: ({ navigation }) => ({
 //             headerRight: MenuIcon(navigation)
 //         })
 //     }
 // };


const {width} = Dimensions.get("window")
const Drawer = createDrawerNavigator({
  List: {
    screen: ListScreen,
    navigationOptions: () => ({
         // title:'titleList',
       drawerLabel: 'List'
    })

  },
  Favs:{
    screen: FavouritesScreen,
    navigationOptions: () => ({
      drawerLabel:'Favourites',
      headerTitle: "Home",
      headerLeft: <Icon name='menu' size={24} color='red'
   onPress={()=> navigation.toggleDrawer()}
   />
    })
  },
    Top: {
      screen: TopScreen,
      navigationOptions:() => ({
        drawerLabel:'Top'
      })
    }
},
{
  drawerPosition: 'left',
  drawerWidth: (width/3) * 2,
  initialRouteName:'List',
  contentOptions:{
   activeTintColor:'#e91e63'
}

})

const App = createAppContainer(Drawer)

export default class RootComponent extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

registerRootComponent(RootComponent);
