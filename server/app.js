require('dotenv').config();

const express = require('express');
const app = express();
const path = require('path');
const ngrok = require('ngrok');
const user = process.env.USERP;
const password = process.env.PASSWORD;
const routes = require('./routes');

app.use(function(req, res, next) {
  res.contentType('application/json');
  next();
});

app.use('/',routes);




const server = app.listen(process.env.PORT, () => {
    console.log('Express listening at ', server.address().port);
})

// ngrok.connect({
//     proto : 'http',
//     addr : process.env.PORT,
//     // auth : `${user}:${password}`
// }, (err, url) => {
//     if (err) {
//         console.err('Error while connecting Ngrok');
//         return new Error('Ngrok Failed');
//     } else {
//         console.log('Tunnel Created -> ', url);
//         console.log('Tunnel Inspector ->  http://127.0.0.1:4040');
//     }
// });
