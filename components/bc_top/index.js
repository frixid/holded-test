/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView
} from 'react-native';

import Header from './../Header'

export default class Top extends Component {
  render() {

    return (
      <SafeAreaView style={styles.container}>
        <Header navigation={this.props.navigation}/>
        <Text>I'm the Top component</Text>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
