/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  SafeAreaView
} from 'react-native';
import { Icon } from 'react-native-elements'
import { DrawerActions } from 'react-navigation-drawer';

export default class Header extends Component {
  render() {
    return (
    <SafeAreaView style={styles.container}>
        <Icon
        iconStyle={{
          justifyContent:'flex-start'
        }}
        name='dehaze'
        onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  burger:{

    },

  container: {
    flex: 1,
     alignItems:"center",
    paddingHorizontal: 10,
    color: 'black',
     // left: 0,
    flexDirection: 'row',
      // right: 0,
     backgroundColor: 'white',
    // justifyContent: 'space-between',
  },
});
