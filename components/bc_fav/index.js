/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  SafeAreaView
} from 'react-native';

import { DrawerActions } from 'react-navigation-drawer';
import Header from './../Header'
// <SafeAreaView style={styles.safeArea} forceInset={{ top: 'never' }}>
export default class Favourite extends Component {
  // static navigationOptions = {
  //   drawerLabel:'Favourites',
  //   // drawerIcon: ({tintColor})= (
  //   //   <Image
  //   //   source={require('./fav-icon.png')}
  //   //   style={[styles.icon,{tintColor:tintColor}]}
  //   //   />
  //   // )
  // }

  render() {

    console.log(this.props);
    return (
      <SafeAreaView style={styles.container}>

      <Header navigation={this.props.navigation}/>

      <View style={styles.containerContent}>
      <Text
      onPress={ () => this.props.navigation.dispatch(DrawerActions.openDrawer())}>
      I'm the Favourite component
      </Text>
      </View>

     </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingLeft:10,
  },
  containerContent:{
    color: "#f9fd0d",
    flex:1,
    backgroundColor: 'purple'


  }
});
