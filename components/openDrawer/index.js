/* @flow */

import React, { Component } from 'react';
import {SafeAreaView, Button, View, Text, Dimensions, ScrollView, StyleSheet} from 'react-native'
import {createDrawerNavigator, DrawerItems,NavigationDrawerProp} from 'react-navigation-drawer' //align

export default class openDrawer extends Component {
  // construct(props){
  //   super(props)
  // }

  // <DrawerItems {...props} />


  static navigationOptions = {
      drawerLabel: 'Home',
      title:'titleList',

    };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView>
        <View style={styles.container}>
          <Text>I'm the openDrawer component</Text>
          <DrawerItems {...props} />
        </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
