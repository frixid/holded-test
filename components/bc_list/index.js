import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  FlatList,
  SafeAreaView
} from "react-native";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions";

import Header from './../Header'

// ssas
// import openDrawer from '/components/openDrawer'

class List extends React.Component {
  constructor(props) {
    super(props);
  }

  // _onPressButton = () => {
  //   this.props.actions.showList();
  // };
  componentDidMount(){
    this.props.actions.showList();

    // this.getData();
  }

  render() {
    const { allCrypto } = this.props;
    // const list = allCrypto.map()
    return (

      <SafeAreaView style={styles.container}>
      <Header navigation={this.props.navigation}/>
      <View style={styles.headerView}>
      <Text style={styles.listTitle}>LIShhTs</Text>
      <View>

            <FlatList
              data={allCrypto
                }
              renderItem={
                ({item}) => <View style={styles.item}>
                <Text>{item.symbol}</Text>
                <Text>{item.askPrice}</Text>
                </View>
              }
               />
      </View>
      </View>
    </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 8,
     // alignItems: "center",
    // justifyContent: "space-around",
    // marginLeft: 20,
    // marginRight: 20
  },
  headerView: {
    flex: 18,
    // justifyContent: "space-around",
     padding: 20,
    backgroundColor:'yellow',
    // marginRight: 20
  },

  item: {
    // padding: 10,
    flex:1,
    fontSize: 24,
    flexDirection:"row",
    justifyContent: 'flex-start',
    color:"blue",

    height:20

    // height: 44,
  },
  listTitle: {
    fontSize: 30,
    color: "black"
  }
});



const mapStateToProps = state =>({
  allCrypto:state.appReducer.allCrypto
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions,dispatch)
})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(List);
