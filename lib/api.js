"use strict";
import "whatwg-fetch";

// const host = "127.0.0.1:5000";
const host = "http://127.0.0.1:5000";
// const host = "https://2c0a3945.ngrok.io"

function request(method, endpoint, params, body, hostname) {
  var endpointURL = hostname + endpoint + params;
  if (!method || !endpointURL) return reject("Missing params (API.request).");
  try {
    return sendRequest(method, endpointURL, body);
  } catch (error) {
    throw error;
  }
}

function sendRequest(method, endpointUrl, body) {
  const req = {
    method: method.toUpperCase(),
    headers: {
      "Content-Type": "application/json"
    }
  };



  
  if (body) req.body = JSON.stringify(body);
  return fetch(endpointUrl, req)
    .then(checkStatus)
    .then(extractJson);
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.status);
  error.response = response;
  throw error;
}

const extractJson = response => response.json();
const API = {
  getCripto: key =>
    // request("GET", "/favCrypto", "?key=" + encodeURIComponent(key), "", host),
    request("GET", "/allCrypto", "", "", host)

};

export default API;
